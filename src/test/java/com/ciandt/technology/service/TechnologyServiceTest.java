package com.ciandt.technology.service;

import com.ciandt.technology.TechnologyApp;
import com.ciandt.technology.domain.Technology;
import com.ciandt.technology.reposity.TechnologyRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = TechnologyApp.class)
public class TechnologyServiceTest {

    private static final String UPDATED_DESCRIPTION = "complete description";


    @Autowired
    private TechnologyService technologyService;

    @Autowired
    private TechnologyRepository technologyRepository;

    private Technology tech;

    @Before
    public void init() {
        technologyRepository.deleteAll();;

        tech = new Technology();
        tech.setId("angular_js");
        tech.setName("Angular JS");
        tech.setShortDescription("Modern web framework");
    }


    @Test
    public void addTechnologyWhenNotExists() {
        technologyRepository.save(tech);

        Optional<Technology> updated = technologyService.findById(tech.getId());
        updated.orElse(null).setDescription(UPDATED_DESCRIPTION);

        technologyService.addOrUpdate(updated.get());

        Optional<Technology> found = technologyService.findById(tech.getId());
        assertThat(found.orElse(null).getDescription(), is(equalTo(UPDATED_DESCRIPTION)));
    }

}
