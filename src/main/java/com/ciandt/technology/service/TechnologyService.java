package com.ciandt.technology.service;

import com.ciandt.technology.domain.Technology;
import com.ciandt.technology.reposity.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TechnologyService {

    @Autowired
    private TechnologyRepository technologyRepository;

    public Technology addOrUpdate(Technology technology) {
        return technologyRepository.save(technology);
    }

    public List<Technology> findAll() {
        return technologyRepository.findAll();
    }

    public Optional<Technology> findById(String id) {
        return technologyRepository.findById(id);
    }

    public void remove(String id) {
        technologyRepository.deleteById(id);
    }


}
