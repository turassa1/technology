package com.ciandt.technology.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Endorsement {
    private Author endorser;
    private Author endorsed;
    private Date timestamp;
    private Technology technology;
}
