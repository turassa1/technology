package com.ciandt.technology.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class Skill {
    private Long id;
    private Technology technology;
    private Author author;
    private Integer value;
    private Date creationDate;
}
